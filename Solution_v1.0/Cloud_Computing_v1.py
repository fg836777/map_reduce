#!/usr/bin/env python
# coding: utf-8

# #### Task 1: Determine the total number of flights from each airport

# In[1]:


#Define data handling functions
#ONE
#collect passenger data
def readPassengerData(data_path):
    #store index of duplicate values
    dups = []
    #store index of invalid rows
    invalid = []
    passenger_info = []
    info = set() #get unique customer info
    error_info = set() #get unique error info
    print('\n')
    print("\n==========================================Passenger Data===========================================")
    print("===================================================================================================")

    with open(data_path, "r") as reader:
        row = reader.readline()
        row_num = 1

        while row:
            row = row.strip()
            row_num +=  1
            # check for unique entries
            if row in info:
                #store the duplicate rows
                x_dup = row_num
                dups.append(x_dup)
    
            #check the data patterns
            else:
                columns = row.split(",")
                if not ValidatePassengerInformation(columns):
                    x_inv = row_num
                    #add to the invalid list
                    invalid.append(x_inv)
                else:
                    passenger_info.append(columns)
                    info.add(row)
            row = reader.readline()
    if len(dups) > 0:
        print("There were ", len(dups), "duplicates in the data\n")
        print("The following row(s) were duplicated\n", dups)
    else:
        print("There were no duplicates in the data")
    print("===================================================================================================")
    if len(invalid) <= 0:
        print("There were no invalid rows in the data")
    else:
        print("There were ", len(invalid), "invalid rows in the data\n")
        print("The following rows were invalid\n", invalid)
    print("===================================================================================================")
    return passenger_info, dups

#TWO: read data related to airports
def readAirportData(data_path):
    #store index of duplicate entries
    dups = []
    #store invalid rows
    invalid = []
    airport_info = []
    info = set() # to store the valid entry row
    print('\n')
    print("\n\n===========================================Airport Data============================================ ")
    print("===================================================================================================")
    with open(data_path, "r") as reader:
        row = reader.readline()
        row_num = 0
        #check row condition to determine whether it is duplicated, invalid, or valid
        while row:
            row = row.strip()
            row_num +=  1
            if row in info:
                x_dup = row_num
                dups.append(x_dup)
            else:
                columns = row.split(",")
                if not CheckFlightEntry(columns):
                    x_inv = row_num
                    invalid.append(x_inv)
                else:
                    airport_info.append(columns)
            row = reader.readline()
    # return airport information, info format: [name, code, latitude, longitude]
    if len(dups) > 0:
        print("There were ", len(dups), "duplicates in the data\n")
        print("The following row(s) were duplicated\n", dups)
    else:
        print("There were no duplicates in the data")
    print("\n===================================================================================================")
    if len(invalid) <= 0:
        print("There were no invalid rows in the data")
    else:
        print("There were ", len(invalid), "invalid rows in the data\n")
        print("The following rows were invalid\n", invalid)    
    print("\n===================================================================================================")
    return airport_info, dups

#THREE
def ValidatePassengerInformation(entry):
    '''Check the info of the passengers'''
    # using regular expression to check the passenger information
    # regular expressions used
    passenger_id = "[A-Z]{3}\d{4}[A-Z]{2}\d" #passenger id
    flight_id = "[A-Z]{3}\d{4}[A-Z]" #flight id
    code = "[A-Z]{3}" #code
    departure_time = "\d{10}" #departure time
    flight_time = "\d{1,4}" #flight time
    if len(entry) !=  6:
        # drop the missing entries
        return False
    validated = ValidateInfo(pattern = passenger_id, s = entry[0]) and ValidateInfo(pattern = flight_id, s = entry[1]) and ValidateInfo(pattern = code, s = entry[2]) and ValidateInfo(pattern = code, s = entry[3])            and ValidateInfo(pattern = departure_time, s = entry[4]) and ValidateInfo(pattern = flight_time, s = entry[5])
    return validated


# validate passenger info
def ValidateInfo(pattern, s):
    pattern = "\A" + pattern + "\Z"
    return re.match(pattern, s)

#validate flight data
def CheckFlightEntry(entry):
    '''Checks whether the flight info is valid or not'''
    # drop the missing entries from the data
    if len(entry) !=  4:
        return False

    name = "[A-Z]\s?{3,20}"
    code = "[A-Z]{3}"
    deg = "-?\d+\.\d+"

    # check the info format to determine if it is correct or not
    return ValidateInfo(code, s = entry[1]) and ValidateInfo(deg, s = entry[2]) and ValidateInfo(deg, s = entry[3]) and len(entry[2]) <= 13 and len(entry[3]) <= 13

def WriteToFile(info, output_path):
   # write dataframe to file
    try:
        import os
        os.mkdir("output/")
    except:
        pass
    info.to_csv(output_path, index = False)


# In[2]:


#Modify file
def modify_time(filename):
    import pandas as pd
    ###Add column names and mofify the time
    csv = pd.read_csv(filename, header = None,
                      names=['Passenger_id', 'Flight_id', 'From', 'To', 'Departure_time', 'Arrival_time','flight_time'])
    csv['Departure_time'] = pd.to_datetime(csv['Departure_time'], unit = 's', origin = pd.Timestamp('1970-1-1'))
    try:
        csv.to_csv('output/flight_info_1.csv', index=False, header=False)
    except:
        os.mkdir('output')
    csv.to_csv('output/flight_info_1.csv', index = False, header = False)


# In[3]:


print('========================================================Task 1========================================================')
#### Task 1
def FlightNumber(passengerEntries, airportEntries):
    '''calculates the number of flights  from each airport'''
    import pandas as pd
    '''map'''
    airport_flight = dict()
    # passenger entries
    for entry in passengerEntries:
        flight_id = entry[1]
        from_code = entry[2]
        if from_code in airport_flight:
            airport_flight[from_code].append(flight_id)
        else:
            airport_flight[from_code] = [flight_id]
    '''reduce'''
    airport_flight_info = dict()
    for entry in airportEntries:
        airport_code = entry[1]
        airport_flight_info[airport_code] = entry + ["0"]
    for airport_code in airport_flight:
        flight_num = len(airport_flight[airport_code])
        if airport_code in airport_flight_info:
            airport_flight_info[airport_code][-1] = str(flight_num)
        else:
            airport_flight_info[airport_code] = ["", airport_code, "", "", str(flight_num)]

    airport_flight_info_number = airport_flight_info.values()
    #store data to a dataframe
    airport_flight_info = pd.DataFrame(airport_flight_info_number)
    airport_flight_info.columns = ['Name', 'Code', 'Latitude', 'Longitude', "Number of Flights"]
    #convert the number of flights to numeric for sorting
    airport_flight_info['Number of Flights'] = pd.to_numeric(airport_flight_info['Number of Flights'])
    airport_flight_info = airport_flight_info.sort_values("Number of Flights", ascending = False)

    return airport_flight_info


# In[4]:


import re
# 1) Import both the flight and passenger data
#passenger
passengerEntries, dups=readPassengerData("Data/AComp_Passenger_data_no_error.csv")
#airport
airportEntries, dups=readAirportData("Data/Top30_airports_LatLong.csv")


# In[ ]:





# In[5]:


# 2) Compute the number of flights from each airport
airport_flight_info = FlightNumber(passengerEntries, airportEntries)
#write to csv
airport_flights = "output/number_of_flights.csv"
WriteToFile(airport_flight_info, airport_flights)
#overview of the number of flights from each airport where the last column is the number of flights
print("\n=========Number of flights per airport (sorted by number of flights)========\n")

print(airport_flight_info)
print('============================================================================')

