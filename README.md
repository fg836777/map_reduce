# CSMBD21 - Cloud Computing Coursework 

This coursework focuses on implementing a non-MapReduce executable prototype using python such that it mimics the functionality of a MapReduce/Hadoop framework whose objectives include:
- To determine the total number of flights from each airport
- To determine the passenger having had the highest number of flights.

## Solution

1. The final version of the solution is in the directory --  **_"Final_Solution/"_**
Under this directory, to view the source code, click on any of the below files --
- **_Cloud_Computing_final_v2.iypnb_**  ( This is jupyter notebook)
 OR
- **_Cloud_Computing_final_v2.py_** (This is .py file)

** **NOTE:** Both the above files contain same source code, just the file format is different for viewing pereferences of users. 

2. Under "Final_Solution/", you will also see two CSV files. These are output files for each tasks.
- **_"number_of_flights.csv"_** ( Task-1 output)
- **_"passenger_flights.csv"_** ( Task-2 output)

